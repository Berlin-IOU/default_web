/*******************************************************************************
 * Copyright (c) 2010, 2030 www.itlaobing.cn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *******************************************************************************/
package cn.itlaobing.servlets;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itlaobing.annoation.RequestMapping;

/** 
 * ClassName: BaseServlet <br/> 
 * Function:  servlet父类 <br/> 
 * date: 2017年10月31日 下午8:47:55 <br/> 
 * 
 * @author 杨乐
 * @version  
 * @since JDK 1.8 
 */
public class BaseServlet extends HttpServlet {
	//定义map用来存放方法名和要执行的操作
	private Map<String, Method> requestMappings=new HashMap<String,Method>();
	@Override
	public void init(ServletConfig config) throws ServletException {
		//获取当前类的class对象
		Class clzServlet=this.getClass();
		//获取webservlet
		WebServlet webservlet=(WebServlet)clzServlet.getAnnotation(WebServlet.class);
		//获取URL
		String baseurl=webservlet.value()[0];
		//反射得到所有的方法
		Method[] method=clzServlet.getDeclaredMethods();
		for (Method method2 : method) {
			//查看方法上面是否有annoation
			if(method2.isAnnotationPresent(RequestMapping.class)) {
				RequestMapping requestMapping=method2.getAnnotation(RequestMapping.class);
				//获取url
				String url=requestMapping.value();
				if(!url.startsWith("/")) {
					url=url+"/";
				}
				String fullurl=baseurl.replace("/*", "")+url;
				//保存到map
				requestMappings.put(fullurl, method2);
			}
		}
		System.out.println(requestMappings);
	}
	
	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#service(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//获得当前请求的url
		String requesturl=request.getRequestURI();
		Method method=requestMappings.get(requesturl);
		if(method==null) {
			response.sendError(404,"请求路径错误！");
		}
		try {
			method.invoke(this,request,response);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}
}
