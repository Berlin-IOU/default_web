/*******************************************************************************
 * Copyright (c) 2010, 2030 www.itlaobing.cn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *******************************************************************************/
package cn.itlaobing.servlets;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itlaobing.annoation.RequestMapping;

/** 
 * ClassName: DefaultServlet <br/> 
 * Function: TODO 功能描述 <br/> 
 * date: 2017年10月31日 下午9:28:04 <br/> 
 * 
 * @author 杨乐
 * @version  
 * @since JDK 1.8 
 */
@WebServlet(value="/default/*",loadOnStartup=1)
public class DefaultServlet extends BaseServlet{
	/** 
	 * index:(查询方法). <br/>
	 * 
	 * @author 杨乐 
	 * @param request
	 * @param response 
	 * @since JDK 1.8 
	 */  
	@RequestMapping("/index")
	public void index(HttpServletRequest request,HttpServletResponse response) {
		System.out.println("查询方法！");
	}
	/** 
	 * create:(新建方法). <br/>
	 * 
	 * @author 杨乐 
	 * @param request
	 * @param response 
	 * @since JDK 1.8 
	 */  
	@RequestMapping("/create")
	public void create(HttpServletRequest request,HttpServletResponse response) {
		System.out.println("新建方法！");
	}
	/** 
	 * update:(更新方法). <br/>
	 * 
	 * @author 杨乐 
	 * @param request
	 * @param response 
	 * @since JDK 1.8 
	 */  
	@RequestMapping("/update")
	public void update(HttpServletRequest request,HttpServletResponse response) {
		System.out.println("更新方法！");
	}
	/** 
	 * delete:(删除方法). <br/>
	 * 
	 * @author 杨乐 
	 * @param request
	 * @param response 
	 * @since JDK 1.8 
	 */  
	@RequestMapping("/delete")
	public void delete(HttpServletRequest request,HttpServletResponse response) {
		System.out.println("删除方法！");
	}
}
