/*******************************************************************************
 * Copyright (c) 2010, 2030 www.itlaobing.cn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *******************************************************************************/
package cn.itlaobing.annoation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/** 
 * ClassName: RequestMapping <br/> 
 * Function: annotation <br/> 
 * date: 2017骞�10鏈�31鏃� 涓嬪崍7:31:26 <br/> 
 * 
 * @author 杨乐
 * @version  
 * @since JDK 1.8 
 */
@Retention(RetentionPolicy.RUNTIME)//运行时可用
@Target(ElementType.METHOD)//只用于方法
public @interface RequestMapping {
	String value() default "";
}
